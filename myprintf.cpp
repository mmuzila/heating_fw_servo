#include <stdio.h>
#include <avr/io.h>

#include "myprintf.h"

/* Builtin printf seems to be buggy. This approach semms to be working.
 * At least fot now... 
 *
 * Well, the problem is probably in puts. It adds newlines...
 *
 * */
void myprintf(const char *fmt, ...)
{
    va_list arg;
    char c;
    va_start(arg, fmt);

    char buf[PRINT_BUF_SIZE];
    vsnprintf(buf, PRINT_BUF_SIZE, fmt, arg);

    va_end(arg);

    for (int i = 0; i < PRINT_BUF_SIZE; i++)
    {
        c = buf[i];
        if (c == '\0')
            return;
        uart_putchar(buf[i], stdout);
    }
}

int uart_putchar(char c, FILE *stream)
{
    if (c == '\n')
        uart_putchar('\r', stream);
    while (LINSIR & (1 << LBUSY));
    LINDAT = c;
    return 0;
}
