#include <string.h>
#include <stdio.h>

#include "can_message.h"
#include "avr_can/avr_can.h"
#include "myprintf.h"



CanMessage::CanMessage(CAN_FRAME _frame)
{
    frame = _frame;

    if (frame.length > 8)
        frame.length = 8;

    if (frame.length < 2)
    {
        frame.length = 1;
        frame.data.byte[0] = (uint8_t) ERROR;
    }

}

uint8_t CanMessage::getCommand()
{
    return frame.data.byte[1];
}

void CanMessage::setCommand(uint8_t command)
{
    frame.data.byte[1] = command;
}

CanMessage::CanMessage(uint32_t _id, CanMessage::msgType _type, uint8_t _command,
       uint8_t * _data, size_t _len, bool _extended)
{
    if (_len > 6)
        _len = 6;

    frame.id = _id;
    frame.extended = _extended;
    setType(_type);
    setCommand((uint8_t) _command);
    frame.length = _len + 2;
    memcpy(frame.data.byte + 2, _data, _len);
}

enum CanMessage::msgType CanMessage::getType()
{
    return (enum msgType) frame.data.byte[0];
}

void CanMessage::setType(enum CanMessage::msgType type)
{
    frame.data.byte[0] = (uint8_t)type;
}

void CanMessage::setResp()
{
    frame.id |= 0x10000000;
}


void CanMessage::print(bool received, bool success)
{
    myprintf("%s", success ? "  " : "! ");
    myprintf("%s  ", received ? "->" : "<-");
    myprintf("can  %08lX   ",  frame.id);
    myprintf("[%d]  ", frame.length);
    for (int count = 0; count < frame.length; count++) {
        myprintf(" %X", frame.data.bytes[count]);
    }
    myprintf("\n");
}
