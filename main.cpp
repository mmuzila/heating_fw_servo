extern"C"
{
#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>
#include <avr/eeprom.h>
}

#include "avr_can/avr_can.h"
#include "can_message.h"
#include "myprintf.h"


#define BAUD 9600

#define PIN_MOTOR_1 PB0
#define PIN_MOTOR_2 PB1

#define PIN_SW_1    PC5
#define PIN_SW_2    PC0

#define PIN_LED_1   PB6
#define PIN_LED_2   PB7
#define PIN_LED_4   PB5

#define PIN_END_OPEN    PD0
#define PIN_END_CLOSE   PD1

#define CAN_DISC_ID     0x00000000
#define CAN_MASK_UNI    0x1FFFFFFF

#define COMMAND_DISC_REQ 0
#define COMMAND_DISC_RES 1

#define COMMAND_VALVE_MOVE  0
#define COMMAND_VALVE_MOVE_OPEN     1
#define COMMAND_VALVE_MOVE_CLOSE    0
#define COMMAND_VALVE_MOVE_ERROR    255
#define COMMAND_VALVE_MOVE_NSUCC    254
#define COMMAND_ERROR       255


#define DEVICE_TYPE     1

#define BIT(x) (1 << x)

const uint32_t can_id_ee EEMEM = CAN_ID;
uint32_t can_id;

void init_uart( void)
{
    LINCR = (1 << LSWRES);
    LINBRRH = (((F_CPU/BAUD)/16)-1)>>8;
    LINBRRL = (((F_CPU/BAUD)/16)-1);
    LINBTR = (1 << LDISR) | (16 << LBT0);
    LINCR = (1<<LENA)|(1<<LCMD2)|(1<<LCMD1)|(1<<LCMD0);
}

static inline void motor_stop(void)
{
    PORTB |= BIT(PIN_MOTOR_1);
    PORTB &= ~BIT(PIN_MOTOR_2);
}

static inline void motor_close(void)
{
    PORTB &= ~BIT(PIN_MOTOR_1);
    PORTB &= ~BIT(PIN_MOTOR_2);
}

static inline void motor_open(void)
{
    PORTB &= ~BIT(PIN_MOTOR_1);
    PORTB |= BIT(PIN_MOTOR_2);
}

void init_motor(void)
{
    motor_stop();
    DDRB |= BIT(PIN_MOTOR_1) | BIT(PIN_MOTOR_2);
}

void led_blink(void (*led_fnc)(void), uint16_t time)
{
    // set prescaller to 1024
    TCCR1B = BIT(CS12) | BIT(CS10);

    // output compare register
    OCR1A = (F_CPU >> 10) * time;

    // set counter to 0
    TCNT1 = 0;

    // clear pending interrupts
    TIFR1 = BIT(OCF1A);

    // enable on compare match A interrupts
    TIMSK1 = BIT(OCIE1A);

    led_fnc();
}


static inline void led_off(void)
{
    PORTB &= ~BIT(PIN_LED_1);
    PORTB |= BIT(PIN_LED_2) | BIT(PIN_LED_4);
}

static inline void led_green(void)
{
    PORTB |= BIT(PIN_LED_2) | BIT(PIN_LED_4);
    PORTB |= BIT(PIN_LED_1);
}

static inline void led_red(void)
{
    led_off();
    PORTB &= ~BIT(PIN_LED_2);
    PORTB |= BIT(PIN_LED_1);
}

static inline void led_blue(void)
{
    led_off();
    PORTB &= ~BIT(PIN_LED_4);
    PORTB |= BIT(PIN_LED_1);
}

void init_led(void)
{
    led_off();
    DDRB |= BIT(PIN_LED_1) | BIT(PIN_LED_2) | BIT(PIN_LED_4);
}

ISR(TIMER1_COMPA_vect)
{

    // disable timer interruprs
    TIMSK1 = 0;
    led_off();
}

void init_buttons(void)
{
    PORTC |= BIT(PIN_SW_1) | BIT(PIN_SW_2);
}

void init_endstops(void)
{
    PORTD |= BIT(PIN_END_OPEN) | BIT(PIN_END_CLOSE);
}

static inline bool is_open(void)
{
    return !(PIND & BIT(PIN_END_OPEN));
}

static inline bool is_closed(void)
{
    return !(PIND & BIT(PIN_END_CLOSE));
}

int valve_move(bool state)
{
    for(int i = 0; i < 300; i++)
    {
        motor_stop();
        if (state)
        {
            if (is_open())
                return 0;
            motor_open();
        }
        else
        {
            if (is_closed())
                return 0;
            motor_close();
        }
        _delay_ms(10);
    }

    return 1;
}

int valve_command_move(CanMessage & msg)
{
    uint8_t orig = msg.data[0];

    if (msg.frame.length < 3)
    {
        msg.frame.length++;
        msg.data[0] = COMMAND_VALVE_MOVE_ERROR;
        return 0;
    }

    switch(msg.data[0])
    {
        case COMMAND_VALVE_MOVE_OPEN:
            if (valve_move(true))
                msg.data[0] = COMMAND_VALVE_MOVE_NSUCC;
            break;

        case COMMAND_VALVE_MOVE_CLOSE:
            if (valve_move(false))
                msg.data[0] = COMMAND_VALVE_MOVE_NSUCC;
            break;

        default:
            msg.data[0] = COMMAND_VALVE_MOVE_ERROR;
            break;
    }

    return msg.data[0] == orig;
}

void command_handler(CAN_FRAME *frame)
{
    bool resp_result;
    led_blink(led_green, 200);

    CanMessage msg = CanMessage(*frame);
    msg.print();

    if (msg.getType() != CanMessage::VALVE)
    {
        msg.setType(CanMessage::ERROR);
        goto resp;
    }

    switch(msg.getCommand())
    {
        case COMMAND_VALVE_MOVE:
            valve_command_move(msg);
            break;

        default:
            msg.setCommand(COMMAND_ERROR);
            break;
    }

resp:
    msg.setResp();
    resp_result = Can0.sendFrame(msg.frame);
    if (resp_result)
        led_blink(led_red, 200);
    msg.print(false, resp_result);

}


void discovery_handler(CAN_FRAME *frame)
{
    bool resp_result;
    uint8_t dev_type = CanMessage::VALVE;

    led_blink(led_blue, 200);

    CanMessage msg = CanMessage(*frame);
    msg.print();

    if (msg.getType() != CanMessage::DISC
            || msg.getCommand() != COMMAND_DISC_REQ)
        return;


    CanMessage msg_resp = CanMessage(can_id, CanMessage::DISC, COMMAND_DISC_RES,
            &dev_type, 1);
    msg_resp.setResp();
    resp_result = Can0.sendFrame(msg_resp.frame);
    if (resp_result)
        led_blink(led_red, 200);
    msg_resp.print(false, resp_result);
}

void init_can(void)
{
    Can0.begin(CAN_BPS_100K);

    //This sets each mailbox to have an open filter that will accept extended
    //or standard frames
    // Use all the mailboxes for receiving.

    Can0.setNumTXBoxes(4);

    Can0.setRXFilter(0, can_id, CAN_MASK_UNI, true);
    Can0.setCallback(0, command_handler);
    Can0.setRXFilter(1, CAN_DISC_ID, CAN_MASK_UNI, true);
    Can0.setCallback(1, discovery_handler);

    //Can0.setGeneralCallback(discovery_handler);

    // Set up the remaining MObs for standard messages.
     //while (Can0.setRXFilter(0, 0, false) > 0) ;
}


bool is_sw1()
{
    return (PINC & BIT(PIN_SW_1)) == 0;
}

bool is_sw2()
{
    return (PINC & BIT(PIN_SW_2)) == 0;
}


int main(void) {

    init_led();
    led_blue();
    _delay_ms(200);
    led_red();
    _delay_ms(200);
    led_off();

    while (!eeprom_is_ready());

    can_id = eeprom_read_dword(&can_id_ee);

    led_green();
    _delay_ms(200);
    led_off();

    sei();

    init_uart();
    fdev_setup_stream(stdout, uart_putchar, NULL, _FDEV_SETUP_WRITE);
    init_can();

    init_motor();
    init_buttons();
    init_endstops();


    myprintf("\ec\e[2JStarting\n");

    for(uint16_t i = 0;; i++)
    {
        bool move = (0x00c0 & i) == 0;
        if (is_sw1() && !is_open())
        {
            if (move)
                motor_open();
            else
                motor_stop();
            led_green();
        }

        else if (is_sw2() && !is_closed())
        {
            if (move)
                motor_close();
            else
                motor_stop();
            led_red();
        }
        else
        {
            motor_stop();
            led_off();
        }
    }

    return 0;
}
