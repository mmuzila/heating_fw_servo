CC=avr-g++
LD=avr-g++
AR=avr-ar

F_CPU=8000000UL

CAN_ID = 0x0FFFFFFDULL


NAME=main
OBJS=main.o #opentherm.o
OUT=$(NAME).elf
MCU=atmega32m1


CFLAGS = -gstabs \
		 -fpack-struct \
		 -Wall \
		 -Wextra \
		 -std=gnu++11 \
	     -mmcu=$(MCU) \
		 -DF_CPU=$(F_CPU) \
		 -Os \
		 -funsigned-char -funsigned-bitfields -fpack-struct -fshort-enums


#LDFLAGS=-Wl,--undefined=_mmcu,--section-start=.mmcu=0x910000 -mmcu=atmega328p\
		#-DF_CPU=$(F_CPU)
#LDFLAGS+=-L $(LPATH)


.PHONY: clean $(NAME) prog reset set_fuses eeprom_prog

main.hex: main.elf
	avr-objcopy -O ihex -R .eeprom $< $@

main.eep: main.o
	avr-objcopy -j .eeprom --set-section-flags=.eeprom="alloc,load" --change-section-lma .eeprom=0 -O ihex $< $@

main.elf: main.o avr_can.o can_message.o myprintf.o
	$(LD) -mmcu=$(MCU) $(LDFLAGS) $^ -o $@

main.o: main.cpp avr_can/avr_can.h can_message.h myprintf.h
	$(CC) $(CFLAGS) -DCAN_ID=$(CAN_ID) -c -o $@ $<

myprintf.o: myprintf.cpp myprintf.h
	$(CC) $(CFLAGS) -c -o $@ $<

can_message.o: can_message.cpp can_message.h avr_can/avr_can.h myprintf.h
	$(CC) $(CFLAGS) -c -o $@ $<

avr_can.o: avr_can/avr_can.cpp avr_can/avr_can.h
	$(CC) $(CFLAGS) -c -o $@ $<

eeprom_prog: main.eep
	avrdude -c usbasp -p m32m1 -U eeprom:w:$<:i

prog: main.hex eeprom_prog
	avrdude -c usbasp -p m32m1 -U flash:w:$<:i

set_fuses:
	avrdude -c usbasp -p m32m1 -U lfuse:w:0xfd:m -U hfuse:w:0xd7:m -U efuse:w:0xff:m


clean:
	rm -f *.hex *.elf *.o *.eep

reset:
	avrdude -p $(MCU) -c usbasp -B300  -Ulfuse:r:/dev/null:i
