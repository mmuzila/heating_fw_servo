#ifndef MYPRINTF_H
#define MYPRINTF_H

#define PRINT_BUF_SIZE  64

void myprintf(const char *fmt, ...);
int uart_putchar(char c, FILE *stream);

#endif
