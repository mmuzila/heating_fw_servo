#ifndef MESSAGE_H
#define MESSAGE_H

#include "avr_can/avr_can.h"

class CanMessage
{
    public:
        enum msgType
        {
            DISC,
            VALVE,
            ERROR = 255,
        };
        enum msgType getType();
        void setType(enum CanMessage::msgType type);

        uint8_t getCommand();
        void setCommand(uint8_t);
        CanMessage(CAN_FRAME);
        CanMessage(uint32_t _id, enum msgType _type, uint8_t _command,
            uint8_t * _data, size_t _len, bool _extended = true);

        CAN_FRAME frame;
        uint8_t * data = &(frame.data.bytes[2]);
        void print(bool received = true, bool success = true);
        void setResp();

};

#endif
